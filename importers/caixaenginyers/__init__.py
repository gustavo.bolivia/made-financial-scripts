"""
Importer for Caixa Enginyers bank data.

We store one file in Q43 format per year, named bank-YYYY.Q43.
This has two exceptions:
- The first few months of 2013 are in bank-2013-start.csv due to the bank
  not being able to provide old movements in Q43 format
- The current year is named bank-current.Q43 and is periodically overwritten
  with fresh information we rename it at the end of the year.

Q43 files are following the Spanish Norma43 format, defined here:
https://empresas.bankinter.com/stf/plataformas/empresas/gestion/ficheros/formatos_fichero/norma_43_ingles.pdf

It is an annoying format with no fixed ids on the single movements, so we have
to synthetize the IDs by concatenating the date and a progressive number for
days with more than one movement (the movements in the files are always in
chronological order).
We also know that no information for a single date is ever split over separate
files.
"""

import csv
from datetime import date, datetime
import re
import pprint
import logging
from os import path
from decimal import Decimal

from beancount.core.number import D
from beancount.core.number import ZERO
from beancount.core.number import MISSING
from beancount.core import data
from beancount.core import account
from beancount.core import amount
from beancount.core import position
from beancount.core import inventory
from beancount.ingest import importer
from beancount.ingest import regression

from ..common import BaseMovement, InternalTransaction
from ..entities import Entities
from specials import bank

__author__ = 'Ugo Riboni <nerochiaro@gmail.com>'

header = "DATA D'OPERACIÓ,CONCEPTE,DATA VALOR,IMPORT,SALDO"
fieldnames = ['DATE', 'DESCRIPTION', 'VALUEDATE', 'AMOUNT', 'BALANCE']

transfers = {
  "TRANSF CTE DE:PayPal Europe S.a": "Assets:PayPal",
  "TRANSF CTE DE:Coinbase UK, Ltd": "Assets:Coinbase:Euro",
  "INGRES EFECTIU": "Assets:Cash"
}

typemap = {
  "TRANSF CTE DE:": "Payment",
  "TRASPAS A COMPTE DE:": "Payment",
  "TRANSFERENCIA A:": "Out",
  "R/ ": "Out",
  "XEC BANCARI ": "Out",
  "COMISSIÓ": "Fee",
  "COMISSIO": "Fee",
  "DEBIT TITOL COOPERATIVA": "Fee",
  "COM.": "Fee",
  "PAGAMENT HISENDA": "Tax",
  "LIQUIDACIO TARGETA": "CreditCard"
}


class Accounts:
    pass
Accounts.root = "Assets:Bank"
Accounts.cash = "Assets:Cash"
Accounts.fees = "Expenses:Fees"
Accounts.members = "Income:Membership"
Accounts.taxes = "Expenses:Taxes"
Accounts.credit = "Liabilities:CreditCard"


class IdGenerator:
    """The bank files do not provide unique IDs in their lines. Therefore we
    create them by using a combination of date and a progressive number for
    movements that happened on the same date (assuming they are in
    chronological order in the file)"""

    def __init__(self):
        self.date = None
        self.prog = None

    def generate(self, date):
        if self.date != date:
            self.date = date
            self.prog = 0
        else:
            self.prog += 1

        return 'BK-' + self.date.strftime('%Y%m%d') + '-' + str(self.prog)


class BankMovement(BaseMovement):
    """Common bank movement object that knows how to create itself
    from lines of a CSV or Q43 files and provides convenient properties"""

    def __init__(self):
        self.source = "BK"
        self.date = None
        self.amount = Decimal(0)
        self.description = ""

    def from_csv_row(row):
        movement = BankMovement()
        movement.date = datetime.strptime(row['DATE'], '%d/%m/%Y').date()
        movement.amount = Decimal(row['AMOUNT'].replace(',', '.'))
        movement.description = row['DESCRIPTION']
        return movement

    def from_q43_row(row):
        movement = BankMovement()
        movement.date = date(int("20" + row[10:12]),
                             int(row[12:14]), int(row[14:16]))
        movement.amount = Decimal(row[28:42]) / 100
        if row[27:28] == "1":
            movement.amount = -movement.amount
        return movement


class BankFile:
    """Class which recognizes the type of file from the name and processes it
    accordingly, returning a sequence of BankMovements"""

    def __init__(self, file):
        self.ids = IdGenerator()
        self.file = file

    def parse(self):
        if path.splitext(self.file.name)[1].lower() == ".csv":
            return self.parse_csv()
        else:
            return self.parse_norma_43()

    def parse_csv(self):
        reader = csv.DictReader(open(self.file.name), fieldnames=fieldnames)
        movements = []
        for i, row in enumerate(reader):
            # Skip header. Please note that the header is necessary as we use
            # it to identify if the file is a bank csv file or a csv file from
            # some other source
            if i == 0:
                continue

            movement = BankMovement.from_csv_row(row)
            movement.id = self.ids.generate(movement.date)
            movements.append(movement)

        return movements

    def parse_norma_43(self):
        movements = []
        with open(self.file.name, encoding='iso8859-1') as f:
            for row in f:
                kind = int(row[0:2])
                if kind == 22:
                    movement = BankMovement.from_q43_row(row)
                    movement.id = self.ids.generate(movement.date)
                    movements.append(movement)
                elif kind == 23:
                    movements[-1].description = row[4:42].strip()
                # TODO: extact initial balance from header row (11) and
                #       insert opening balance statement
                # TODO: extact closing balance from footer row (33) and
                #       insert closing balance statement

        return movements


class Importer(importer.ImporterProtocol):

    def __init__(self, data_root):
        self.currency = "EUR"
        self.data_root = data_root

    def identify(self, file):
        if path.splitext(file.name)[1].lower() == ".csv":
            head = file.contents().split("\n")[0]
            return header == head
        else:
            return path.splitext(file.name)[1].lower() == ".q43"

    def file_name(self, file):
        return path.basename(file.name)

    def file_account(self, _):
        return Accounts.root

    def file_date(self, file):
        name = path.splitext(path.basename(file.name))[0]
        return datetime.strptime(name, 'bank-%Y').date()

    def extract(self, file):
        # Load information about the entities we interact with, which we
        # store in a number of separate csv files.
        entities = Entities(self.data_root)

        movements = BankFile(file).parse()

        entries = []
        index = 0
        for index, movement in enumerate(movements):
            meta = data.new_metadata(file.name, index)
            meta['movement-id'] = movement.id

            transaction = InternalTransaction(movement.date, meta)

            # Sometimes we just want to ignore the movement:
            if bank.ignore(movement):
                continue

            value = amount.Amount(movement.amount, self.currency)

            # Decide type of movement by checking if description starts with
            # specific strings, and if it does parse the rest of the
            # description to add more information to the transaction.
            rtype = None
            source = None
            for i, stamp in enumerate(transfers):
                if movement.description.startswith(stamp):
                    rtype = "Transfer"
                    source = transfers[stamp]
            if rtype is None:
                for i, stamp in enumerate(typemap):
                    if movement.description.startswith(stamp):
                        rtype = typemap[stamp]
                        alias = movement.description[len(stamp):].strip()
                        break

            if rtype == 'Payment':
                # For bank movements there is usually not enough data
                # to be able to automatically categorize the type of
                # income. We rely on bank.manually_categorize() to alter
                # the transaction before it's posted by applying any manual
                # categorization.
                account = "Income:Unknown"
                member = entities.member_by_alias(alias)
                if member is not None:
                    meta['member-id'] = str(member['id'])
                    payee = member['full']
                else:
                    customer = entities.customer_by_alias(alias)
                    if customer is not None:
                        meta['customer-id'] = customer['id']
                        payee = customer['fullname']
                        account = customer['account']
                    else:
                        logging.error("Payment from unknown person: \"%s\" %s",
                                      alias, value)
                        continue

                transaction.payee = payee
                transaction.narrative = movement.description
                transaction.move(-value, account, to=Accounts.root)
            elif rtype == 'Fee':
                # TODO: link fee to related movement, if any
                # (only for movements from q43 files)
                transaction.payee = "Caixa Enginyers"
                transaction.narrative = movement.description
                transaction.move(value, Accounts.root, to=Accounts.fees)
            elif rtype == 'Transfer':
                transaction.narrative = "Transfer between accounts"
                transaction.move(-value, source, to=Accounts.root)
            elif rtype == 'Out':
                customer = entities.customer_by_alias(alias)
                if customer is not None:
                    meta['customer-id'] = customer['id']
                    payee = customer['fullname']
                    account = customer['account']
                else:
                    payee = None
                    logging.error("Payment to unknown person: \"%s\" %s",
                                  alias, value)
                    continue

                transaction.payee = payee
                transaction.narrative = movement.description
                transaction.move(value, Accounts.root, to=account)
            elif rtype == 'Tax':
                transaction.narrative = "Taxes"
                transaction.move(value, Accounts.root, to=Accounts.taxes)
            elif rtype == 'CreditCard':
                cardtype, digits, when = alias.strip().split(" ")
                when = datetime.strptime(when, "%m/%y").date()
                transaction.narrative = "Refund credit card {0} {1} until " \
                                        "{2}".format(cardtype, digits,
                                                     when.strftime("%b %Y"))
                transaction.move(value, Accounts.root, to=Accounts.credit)
            else:
                logging.error("Skipping unknown row type: %s; \"%s\" %s %s",
                              rtype, movement.description, value, movement.id)
                continue

            bank.manually_categorize(movement, transaction)
            entries.append(transaction.to_beancount())

        return entries


def test():
    # Create an importer instance for running the regression tests.
    importer = Importer()
    yield from regression.compare_sample_files(importer, __file__)
