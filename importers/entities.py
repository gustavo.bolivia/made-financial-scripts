"""
Load information about the entities we interact with from a number of CSV files
The files we use are as follows:

members.csv: database of MADE members, downloaded periodically from one of the
             sheets in the membership spreadsheet on Google
aliases.csv: the name of the member involved in a transaction that appears in
             the bank statements is almost always truncated, mangled or
             otherwise different from the real member name.
             Additionally PayPay movements use email addresses that sometimes
             are different from the email the member signed up with.
             Here we maintain a mapping between these different names and the
             actual member id so that we can recognize them in the future.
customers.csv: stores information about our providers and customers, including
               fiscal information and the aliases they use in bank and
               PayPal statements.

These files are currently stored in a path configured together with the
importers.
"""

import csv
import unicodedata
from os import path

MEMBERS_FIELDNAMES = ['stamp', 'first', 'last', 'email', 'phone', 'fiscal_id',
                      'mode', '_', 'id']
ALIASES_FIELDNAMES = ['id', 'alias']


def normalize_name(name):
    """Brutish way to normalize names, by dropping all accented letters and
    replacing them with ASCII ones. Used only for matching purposes."""
    return unicodedata.normalize('NFKD', name)


class Entities:
    def __init__(self, root):
        # TODO: we can probably do a much better job at efficiently creates
        #       these lookup tables, but not a problem in the short term.

        entities_path = path.join(root, "entities")

        filename = path.join(entities_path, 'members.csv')
        members = csv.DictReader(open(filename), fieldnames=MEMBERS_FIELDNAMES)
        filename = path.join(entities_path, 'aliases.csv')
        member_aliases = csv.DictReader(open(filename), strict=True,
                                        fieldnames=ALIASES_FIELDNAMES)

        self.members = {}
        self.member_aliases = {}
        for i, member in enumerate(members):
            if i == 0:
                continue  # skip CSV header

            id = member.get('id', None)
            if id is None:
                continue  # skip empty lines

            # Skip members that have not been assigned an ID yet
            # This is necessary since assigning IDs is manual and subject to
            # human review, since users may sign up twice from the web form and
            # then get deleted, or similar issues.

            id = id.strip()
            if len(id) == 0:
                continue

            id = int(id)

            member['id'] = id
            member['full'] = " ".join([member['first'].strip(),
                                       member['last'].strip()])
            member['searchable'] = normalize_name(member['full'])
            self.members[id] = member

            # email addresses are considered member aliases, and later we
            # add on top of them aliases manually added from the
            # aliases.csv file
            self.member_aliases[member['email'].lower()] = member['id']

        for i, alias in enumerate(member_aliases):
            self.member_aliases[alias['alias'].lower()] = alias['id']

        filename = path.join(entities_path, 'customers.csv')
        customers = csv.DictReader(open(filename))

        self.customers = {}
        self.customer_aliases = {}
        for i, customer in enumerate(customers):
            customer['aliases'] = customer['aliases'].split('|')
            self.customers[customer['id']] = customer
            for alias in customer['aliases']:
                self.customer_aliases[alias] = customer

    def member_by_id(self, id):
        return self.members.get(int(id), None)

    def member_by_alias(self, alias):
        id = self.member_aliases.get(alias.lower(), None)
        return self.member_by_id(id) if id is not None else None

    def customer_by_id(self, id):
        return self.customers.get(id, None)

    def customer_by_alias(self, alias):
        return self.customer_aliases.get(alias, None)
