"""
Importer for PayPal data.

We export movement history from paypal from the start of the year to
the current date into paypal-current.csv.
Historical information from previous years is stored as paypal-YYYY.csv

Please note that when exporting from paypal the format needs to be
"Comma Delimited - All Activity"

Also the following fields need to be enabled in "Customize Download Fields":
"PayPal Website Payments"
    Item ID
"Other Fields"
    Subject
    Note
    Subscription Number
    Payment Type
    Balance Impact
"""

import csv
from datetime import date, datetime
import re
import pprint
import logging
from os import path
from decimal import Decimal, InvalidOperation
from enum import Enum

from beancount.core.number import D
from beancount.core.number import ZERO
from beancount.core.number import MISSING
from beancount.core import data
from beancount.core import account
from beancount.core import amount
from beancount.core import position
from beancount.core import inventory
from beancount.ingest import importer
from beancount.ingest import regression

from ..common import BaseMovement, InternalTransaction
from ..entities import Entities
from specials import paypal

__author__ = 'Ugo Riboni <nerochiaro@gmail.com>'

HEADER = "Date, Time, Time Zone, Name, Type, Status"
FULL_HEADER = HEADER + ", Subject, Currency, Gross, Fee, Net, Note, " \
         "From Email Address, To Email Address, Transaction ID, " \
         "Payment Type,Item ID, Reference Txn ID, Subscription Number, " \
         "Receipt ID, Balance, Balance Impact"

LASER_PURCHASE_CODES = ['laser-donation']
MEMBER_PURCHASE_CODES = ['onemonthsubscribe', 'threemonthsubscribe',
                         'onemonth', 'threemonth']
MEMBER_SUBSCRIBE_CODES = ['Made-Membreship', 'Made Membership']

ROOT_CURRENCY = 'EUR'


class Accounts:
    pass
Accounts.root = "Assets:PayPal"
Accounts.bank = "Assets:Bank"
Accounts.fees = "Expenses:Fees:PayPal"
Accounts.members = "Income:Membership"
Accounts.laser = "Income:Laser"
Accounts.sent_refunds = "Expenses:Refunds"
Accounts.received_refunds = "Income:Refunds"
Accounts.disputed_out = "Expenses:Disputed"
Accounts.disputed_in = "Income:Disputed"


def paypal_number(value):
    return Decimal(str(value).replace('.', '').replace(',', '.'))


class PayPalMovement(BaseMovement):
    """Movement object that knows how to create itself
    from lines of a CSV file and provides convenient properties"""

    def __init__(self):
        self.source = "PP"
        self.date = None
        self.currency = ROOT_CURRENCY
        self.amount = Decimal(0)
        self.net = Decimal(0)
        self.fee = Decimal(0)
        self.email = ""
        self.note = ""
        self.is_income = True
        self.row = None

    def from_csv_row(row):
        movement = PayPalMovement()
        movement.row = str(row)
        movement.date = datetime.strptime(row['Date'], '%d/%m/%Y').date()

        movement.subject = row['Subject']
        movement.item = row['Item ID']
        movement.note = row['Note']

        movement.currency = row['Currency']
        try:
            movement.amount = paypal_number(row['Gross'])
            movement.fee = paypal_number(row['Fee'])
            movement.net = paypal_number(row['Net'])
        except InvalidOperation:
            logging.error("Skipping row with invalid numbers "
                          "(gross, fee, net): %s %s %s",
                          row['Gross'], row['Fee'], row['Net'])

        movement.transaction = row['Transaction ID']
        movement.reference = row['Reference Txn ID']
        if movement.reference == "":
            movement.reference = None

        movement.id = 'PP-' + movement.transaction
        movement.account = None
        movement.type = row['Type']

        movement.is_memo = row['Balance Impact'] == 'Memo'

        if movement.is_memo:
            movement.is_income = movement.amount > 0
        else:
            movement.is_income = row['Balance Impact'] == 'Credit'

        # Store only one email address: the one of the person paying
        # us or being paid by us. We already know the other one anyway
        if movement.is_income:
            movement.email = row['From Email Address']
        else:
            movement.email = row['To Email Address']

        return movement

    def __str__(self):
        return " | ".join([self.id, self.email, self.type, str(self.amount),
                           self.item, self.subject, self.note,
                           self.reference or ""])


class PayPalFile:
    """Class which recognizes the type of file from the name and processes it
    accordingly, returning a sequence of movements.
    """

    FIELD_NAMES = [field.strip() for field in FULL_HEADER.split(",")]

    def __init__(self, file):
        self.file = file

    def parse(self):
        reader = csv.DictReader(open(self.file.name, encoding="ISO-8859-1"),
                                fieldnames=PayPalFile.FIELD_NAMES)

        # first read the file into a list of PayPalMovement...
        movements = []
        for i, row in enumerate(reader):
            # Skip header. Please note that the header is necessary as we use
            # it to identify if the file comes from this source or another
            if i == 0:
                continue

            movement = PayPalMovement.from_csv_row(row)
            movements.append(movement)

        # .. then discard, modify or combine movements that are composed by
        # multiple items.
        # See the Notes section at the end of this file for details on these
        # combined movements.

        def movement_by_id(id):
            results = [m for m in movements if m.transaction == id]
            return results[0] if len(results) > 0 else None

        real_movements = []
        for movement in movements:
            # We want to skip memo movements, as there will be another movement
            # in the stream that refers to the memo, at the actual time when
            # the money is available in the account.
            if movement.is_memo:
                continue

            if movement.currency != ROOT_CURRENCY:
                # Skip all movements that are not in our main currency to hide
                # instantaneous currency conversions (to keep things simpler).
                # See notes at the bottom of this file for details on
                # currency conversion streams and how we handle them.
                continue

            if movement.reference:
                referred = movement_by_id(movement.reference)
                if referred is None:
                    # This might happen when effective movement and memo are
                    # split among historical files, or when the effective
                    # movement has not happened yet at the time of download.
                    # Report error as this needs human intervention (by moving
                    # movements between files, or temporarily excluding them)
                    logging.error("Missing referenced movement: %s => %s ",
                                  movement.id, movement.reference)
                    continue

                # If this movement has no user-submitted notes, copy them
                # from the memo movement, as they usually live there.
                if not movement.note:
                    movement.note = referred.note
                if not movement.subject:
                    movement.subject = referred.subject

                # Currency conversion results don't have the email address
                # of the original sender, so we need to get it from the
                # referred movement.
                if not movement.email:
                    movement.email = referred.email

            real_movements.append(movement)

        return real_movements


class Importer(importer.ImporterProtocol):

    def __init__(self, data_root):
        self.data_root = data_root

    def identify(self, file):
        if path.splitext(file.name)[1].lower() == ".csv":
            head = file.contents().split("\n")[0]
            if head.startswith(HEADER):
                if head.startswith(FULL_HEADER):
                    return True
                else:
                    logging.error("File recognized as PayPal file but "
                                  "missing fields.\nNeed: \"%s\"\n"
                                  "Have: \"%s\"", FULL_HEADER, head)
        return False

    def file_name(self, file):
        return path.basename(file.name)

    def file_account(self, _):
        return Accounts.root

    def file_date(self, file):
        name = path.splitext(path.basename(file.name))[0]
        return datetime.strptime(name, 'paypal-%Y').date()

    def extract(self, file):
        # Load information about the entities we interact with, which we
        # store in a number of separate csv files.
        entities = Entities(self.data_root)

        movements = PayPalFile(file).parse()

        entries = []
        index = 0
        for movement in movements:
            # The index is not accurate since we combined or deleted movements
            # spanning multiple lines. So just put 0 instead of a bogus value.
            # TODO: keep original source line of at least one of the movements
            meta = data.new_metadata(file.name, 0)
            meta['movement-id'] = movement.id

            transaction = InternalTransaction(movement.date, meta)

            gross = amount.Amount(movement.amount, ROOT_CURRENCY)
            net = amount.Amount(movement.net, ROOT_CURRENCY)
            fee = amount.Amount(movement.fee, ROOT_CURRENCY)

            # Transfers between paypal sub-accounts in different currencies
            # are not supported, and we handle them as special cases instead,
            # since historically we only had one before we changed our setup
            # to instant currency conversion. See bottom of file, and the
            # paypal specials module for details.
            paypal.handle_multicurrency_transfers(movement)

            # If a paypal user complains reports a movement as invalid,
            # paypal will put it on hold, i.e. remove it from our balance until
            # the dispute is somehow resolved and the money is returned.
            # I don't know what happenes if the money is not returned, since
            # it never happened to us.
            # Either way, the Reference field will keep track of the dispute
            # chain, so let's make sure to keep it around.
            if movement.type == "Temporary Hold":
                transaction.post(Accounts.disputed_out, -net)
                transaction.post(Accounts.root)
                transaction.narration = "Paypal: " + movement.type
                meta['reference'] = 'PP-' + movement.reference
            elif movement.type == "Update to Reversal":
                transaction.post(Accounts.disputed_in, -net)
                transaction.post(Accounts.root)
                transaction.narration = "Paypal: " + movement.type
                meta['reference'] = 'PP-' + movement.reference

            elif movement.is_income:
                member = entities.member_by_alias(movement.email)

                if member is not None:
                    meta['member-id'] = str(member['id'])
                    payee = member['full']
                else:
                    payee = None

                # If the specials module hasn't categorized this movement, then
                # check which standard case it belongs to, if any
                if movement.account is None:
                    # Movements which have the 'Item ID' field are purchases of
                    # predefined products, like recurring memberships or laser
                    # hour packs.
                    if movement.item:
                        if movement.item in MEMBER_PURCHASE_CODES:
                            movement.account = 'Income:Membership'
                        elif movement.item in LASER_PURCHASE_CODES:
                            movement.account = 'Income:Laser'
                        else:
                            logging.error("Unknown product purchase: %s",
                                          movement)
                            continue

                    # Movements with this specific subject are coming from
                    # membership subscriptions set up through the website
                    elif movement.subject in MEMBER_SUBSCRIBE_CODES:
                        movement.account = 'Income:Membership'

                # After trying to categorize the movements automatically,
                # check if we have added any manual categorization via the
                # specials module.
                paypal.set_account_manually(movement)

                # If still uncategorized, just emit an error
                if movement.account is None:
                    logging.error("Unrecognized movement: %s", movement)
                    continue

                # Membership payments can only come from actual members
                if payee is None and movement.account == Accounts.members:
                    logging.error("Membership from unknown person: %s",
                                  movement)
                    continue

                transaction.payee = payee

                # Set the narrative on the transaction to "Subject: Note:, but
                # only when it's not an automated transaction, otherwise just
                # use the Note.
                if movement.item or movement.subject in MEMBER_SUBSCRIBE_CODES:
                    transaction.narration = movement.note
                else:
                    non_empty = filter(None, [movement.subject, movement.note])
                    transaction.narration = ": ".join(non_empty)

                if movement.account == "Income:Refunds":
                    meta['reference'] = movement.reference

                transaction.post(movement.account, -gross)
                transaction.post(Accounts.root, net)
                if movement.fee != 0:
                    transaction.post(Accounts.fees, -fee)
            else:
                if movement.type == "Withdraw Funds to Bank Account":
                    transaction.post(Accounts.root, net)
                    transaction.post(Accounts.bank)
                elif movement.type == "Refund":
                    transaction.post(Accounts.sent_refunds, -net)
                    transaction.post(Accounts.root)
                    meta['refund-for'] = 'PP-' + movement.reference
                    transaction.payee = payee
                    transaction.narration = movement.note
                else:
                    # We actually purchased something or paid for some service
                    customer = entities.customer_by_alias(movement.email)
                    if customer is not None:
                        meta['customer-id'] = customer['id']
                        transaction.payee = customer['fullname']
                        transaction.post(Accounts.root, net)
                        transaction.post(customer['account'])
                    else:
                        logging.error("Unrecognized expense: %s", movement)
                        continue

            # Some movements are paying for different things at the same
            # time, so we add manual rules to create the correct postings
            # in the transaction, via the specials module.
            paypal.split_combined_movements(movement, transaction)

            entries.append(transaction.to_beancount())

        return entries


def test():
    # Create an importer instance for running the regression tests.
    importer = Importer()
    yield from regression.compare_sample_files(importer, __file__)


"""
=== NOTES ===

The following notes contain some explanation on the automated importer
implemented by this file, by describing how some of the Paypal data streams
in the CSV exports work. This is from direct observation and some information
from various internet sources, but I could not find good documentation directly
from Paypal themselves.

From here on we use the convention where we put the movement Type field
between quotes, then any other fields as (Name: Value).

== Memos ==

Sometimes we receive movement pairs that look like this:

- "Payment Received" (Balance Impact: Memo)
- ... potentially more movements may happen in between ...
- "Update to Payment Received" (Balance Impact: Credit/Debit)

In this case the actual date the money is really present in the account is at
the time of the last movement, but all the information about who paid, notes,
etc. is contained it the Memo movement.

== Currency conversion ==

Currency conversion can work in two ways in paypal:
(a) we have various sub-accounts in different currencies and we manually
    order conversions and transfers between them
(b) we only have one account in our main currency, and any movements in
    different currencies will be converted on the fly

Let's assume our main currency is EUR and we receive USD funds.
In case (a) the stream looks like this:

- "Payment Received" USD
- ... we potentially receive more USD or EUR movements ...
- ... we decide to transfer all or just some of our USD to EUR ...
- "Transfer" (Name: "To Euro") USD
- "Transfer" (Name: "From U.S. Dollar") EUR

The Transfer movements will not reference in any way the various movemements
that contributed to create the USD balance we are transferring (and there is
really no way for this to be possible, especially if we transfer less than
the full balance).

We do not handle case (a) in this importer as this is not useful for us.

However case (b) is handled here, and the stream always looks like this:
- "Payment Received" USD
- "Currency Conversion" (Name: "To Euro") USD
- "Currency Conversion" (Name: "From U.S. Dollar") EUR

Both the "Currency Conversion" movements have a reference to the
"Payment Received" movement, so we can essentially pretend that the transfer
in USD never happened and just act as if received the money in EUR in the
first place.

Please note that the sender information, notes, etc. are all on the original
"Payment Received" movement (or related Memo, see above) so it will need to
be copied.
"""
