from beancount.core import data


class BaseMovement:
    def __init__(self):
        self.source = None
        self.id = None


class InternalPosting:
    def __init__(self, account, units=None, meta=None):
        self.account = account
        self.units = units
        self.meta = meta

    def to_beancount(self):
        return data.Posting(self.account, self.units,
                            None, None, None, self.meta)


class InternalTransaction:
    """Since the Transaction and Posting objects from beancount.core.data
    can only be created by passing all arguments, and then the properties are
    read-only, it does not give us the flexibility we need when working in
    importers.
    Therefore we use this more convenient internal class and generate a
    beancount Transaction at the end of the process"""

    def __init__(self, date, meta=None, payee=None, narration=None):
        self.date = date
        self.payee = payee
        self.narration = narration
        self.meta = meta
        self.postings = []
        self.flag = "*"
        self.tags = None
        self.links = None

    def post(self, account, units=None):
        self.postings.append(InternalPosting(account, units))

    def move(self, value, src, to):
        """Shorthand for simple cases when we move a value from an account to
        another. The value in the target account is left implicit."""
        self.postings.append(InternalPosting(src, value))
        self.postings.append(InternalPosting(to))

    def to_beancount(self):
        postings = [posting.to_beancount() for posting in self.postings]
        return data.Transaction(self.meta, self.date, self.flag,
                                self.payee, self.narration,
                                self.tags, self.links,
                                postings)
